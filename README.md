# My Cool CV

Features

- ✓ creating pdf files
- ✓ including qr codes
- ✓ generation CVs in gitlab pipeline


PDF is uploaded:

- [Forrest Gump.pdf](https://gitlab.com/maxistar/cv/-/jobs/7606815615/artifacts/raw/build/Forrest_Gump.pdf)


## Getting Started

You can close this repo locally, remove .git folder and edit forrest_gump.json as you wish.

```shell

# install a copy of this repo locally
git clone https://gitlab.com/maxistar/cv.git

cd cv

npm i && nmp run build

```

a new folder build will be created with a pdf file in it.



TODO:

- [x] decouple scripts from index
- [x] extract data to json
- generation CVs in GitHub pipeline (coming soon)
- generate the cv using creator (coming soon)
- [ ] there are lots of examples of fictional characters
- [ ] https://www.mrlcg.com/latest-media/10-cvs-from-some-well-known-fictional-faces-279591/
- [ ] https://www.forevergeek.com/cvs-of-famous-fictional-characters/
- [ ] https://enhancv.com/resume-examples/famous/bill-gates/

Build
```
npm install
npm run build
```

## How to use

create json file with CV data


## Road Map
