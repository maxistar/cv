const pdf = require("pdf-creator-node");
const path = require("path");
const QRCode = require("qrcode");
const fs = require("fs");

module.exports = class {
    async generate(data, theme, path) {
        this.data = await this.prepareData(data, theme, path);
        this.generatePdf()
    }
    async createQRCode(url) {
        return new Promise((resolve) => {
            QRCode.toDataURL(
                url,
                (err, url) => {
                    resolve(url);
                }
            )
        });
    }
    resolve(relativePath) {
        if (!relativePath) {
            return null;
        }
        return "file://" + path.resolve(relativePath);
    }
    async prepareData(data, theme, path) {
        return {
            html: fs.readFileSync(`./themes/${theme}/template.html`, 'utf8'),
            data: {...data,
                qrcode: await this.createQRCode(data.qrcode),
                cssEncoded: this.resolve(`./themes/${theme}/style.css`),
                cssFonts: this.resolve("./css/cv.css"),
                portraitUrl: this.resolve(data.portraitUrl),
                experience: data.experience.map(obj => ({
                    ...obj,
                    logo: this.resolve(obj.logo)
                })),
                education: data.education.map(obj => ({
                    ...obj,
                    logo: this.resolve(obj.logo)
                }))
            },
            path
        };
    }
    generatePdf() {
        const today = new Date();
        const date = today.toISOString().split("T")[0];
        const name = this.data.data.name;
        const options = {
            format: "A4",
            orientation: "portrait",
            border: "14mm",
            header: {
                height: "0mm",
                contents: '<header></header>'
            },

            "footer": {
                "height": "10mm",
                "contents": {
                    first: `<footer><div class="left">First page</div><div class="right">${name}</div></footer>`,
                    2: `<footer><div class="left">Second page</div><div class="right">${date} ${name}</div></footer>`, // Any page number is working. 1-based index
                    default: '<span>{{page}}</span>/<span>{{pages}}</span>', // fallback value
                    last: 'Second Page'
                }
            }
        };

        pdf.create(this.data, options)
            .then(res => {
                console.log(res)
            })
            .catch(error => {
                console.error(error)
            });
    }
}
