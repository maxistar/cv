const CVGenerator = require("./src/cvgenerator");
const args = process.argv.slice(2);

let inputFile, outputFile, theme='default';
let currentArg;

for (let i = 0; i < args.length; i++) {
    currentArg = args[i];
    if (currentArg.indexOf('--input=')===0) {
        inputFile = currentArg.substring('--input='.length)
    } else if (currentArg.indexOf('--output=')===0) {
        outputFile = currentArg.substring('--output='.length)
    } else if (currentArg.indexOf('--theme=')===0) {
        theme = currentArg.substring('--theme='.length)
    }
}

async function main(inputFile, outputFile, theme) {
    const cvGenerator = new CVGenerator();
    const data = require(inputFile);
    await cvGenerator.generate(data, theme, outputFile);
}


if (inputFile && outputFile) {
    console.log(`Input file: ${inputFile}`);
    console.log(`Output file: ${outputFile}`);
    console.log(`theme: ${theme}`);

    main(inputFile, outputFile, theme).then(r => console.log(r));
} else {
    console.error('Usage: node index.js --input=inputfile.json --output=outputfile.pdf');
}
